from lib.bayesian_optimization import BayesianOptimization
import numpy as np


def target_func(p1, p2, p3, p4):
    if not isinstance(p3, int) or not isinstance(p4, int):
        raise Exception("p3/p4 should be an integer!")
    if p2 == 'plus':
        return p1 + 2 * p3 * p4
    elif p2 == 'minus':
        return p1 - 2 * p3 * p4
    else:
        raise Exception('p2 is illegal str!')


def converge_func(x, y, sign='&'):
    if sign == '*':
        a = np.sin(1/(x**2))
        return a
    elif sign == '&':
        return -(y-2)**2 + (np.log(x)-1)**2 + 2
    else:
        raise Exception('Invalid sign!')


def branin(x, y):
    result = np.square(y - (5.1 / (4 * np.square(math.pi))) * np.square(x) + (5 / math.pi) * x - 6) + 10 * (1 - (1. / (8 * math.pi))) * np.cos(x) + 10
    result = float(result)
    return -result


parameters = {'p1': {'type': 'cont', 'value': [0, 1]}, 'p2': {'type': 'cate', 'value': ['plus', 'minus']}, 'p3': {'type': 'int', 'value': [4, 8, 16, 32]}, 'p4': {'type': 'int', 'value': range(0, 10)}}


random_state = 0
verbose = 2
BO_object = BayesianOptimization(target_func, parameters, random_state=random_state, verbose=verbose)

BO_object.maximize(init_points=5, n_iter=25, acq='ei', kappa=2.576, xi=0.0)
# Parameters for GaussianProcess Regressor can be added.

max_target = BO_object.max['target']
max_params = BO_object.max['params']
paths = BO_object.res

print("Maximum target is", max_target)
print("Corresponding params are", max_params)


# Converge Func
parameters = {'x': {'type': 'cont', 'value': [1, 10]}, 'y': {'type': 'cont', 'value': [-3.5, 20]}, 'sign': {'type': 'cate', 'value': ['*', '&']}}
BO_object = BayesianOptimization(converge_func, parameters, random_state=random_state, verbose=verbose)

BO_object.maximize(init_points=5, n_iter=25, acq='ei', kappa=2.576, xi=0.0)
# Parameters for GaussianProcess Regressor can be added.

max_target = BO_object.max['target']
max_params = BO_object.max['params']
paths = BO_object.res

print("Maximum target is", max_target)
print("Corresponding params are", max_params)


import time, math
st = time.time()
parameters = {'x': {'type': 'cont', 'value': [-5, 10]}, 'y': {'type': 'cont', 'value': [0, 15]}}
BO_object = BayesianOptimization(branin, parameters, random_state=random_state, verbose=verbose)

BO_object.maximize(init_points=5, n_iter=40, acq='ei', kappa=2.576, xi=0.0)
# Parameters for GaussianProcess Regressor can be added.

max_target = BO_object.max['target']
max_params = BO_object.max['params']
paths = BO_object.res

et = time.time()
print('Total time:', str((et-st)/60)[:6])
print("Maximum target is", max_target)
print("Corresponding params are", max_params)
