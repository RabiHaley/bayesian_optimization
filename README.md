### This is the repository of Bayesian Optimization for UChicago Project Lab.

- 'lib' would be the final package to use and the name will be changed when we finished.
    - The class of Bayesian Optimization has been capsulated as class HyperparamBayesianOptimizer in bayesian_optimization.py.
    - Follow the documentation to use that class.
    - Or you may refer to testing_program_lib_new.py as an example. Testing function is simple, but involve integer-valued and categorical parameters.
- See lib.bayesian_optimization for TODO LIST.
- Please help test the program on CloudQuantAI.
   
    

- The following package has been deleted: modAL and bayesopt.