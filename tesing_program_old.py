import numpy as np
import pandas as pd
import time
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout
from keras.optimizers import Adam, Adagrad, RMSprop
from lib_pre.bayesian_optimization import HyperparamBayesianOptimizer
from lib_pre.acquisition import max_PI


# Target function: Objective of Bayesian Optimization
def target_function(batch_size=32, epoch=100, learning_rate=0.001, dropout=0.5, activation='tanh', optimizer='adam'):
    model = Sequential()
    model.add(LSTM(units=batch_size, input_shape=(X_train.shape[1], 3)))
    model.add(Dropout(dropout))
    model.add(Dense(units=batch_size))
    model.add(Dense(units=1))
    if optimizer == 'adam':
        opt_engine = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    elif optimizer == 'Adagrad':
        opt_engine = Adagrad(lr=learning_rate, epsilon=None, decay=0.0)
    elif optimizer == 'RMSprop':
        opt_engine = RMSprop(lr=learning_rate, rho=0.9, epsilon=None, decay=0.0)
    model.compile(loss='binary_crossentropy', optimizer=opt_engine)
    fit_model = model.fit(X_train, y_train, batch_size=batch_size, epochs=epoch, verbose=0)
    y_predict = model.predict(X_train)
    auc = roc_auc_score(y_train, y_predict)
    return auc


# Data of LSTM model
GOOG = pd.read_csv('GOOG.csv', index_col=0)
# columns 7: Date, Open, High, Low, Close, Adj Close, Volume
SnP = pd.read_csv('SP_index.csv', index_col=0)
dataset = GOOG.loc[:, ['Adj Close', "Volume", 'Open']]
dataset.dropna(inplace=True)  # len: 2051
y_data = pd.concat([dataset['Adj Close'], SnP['Adj Close']], axis=1, sort=True, ignore_index=True)
y_data.columns = ['Google', 'S&P_index']
y_data = y_data.pct_change()
y_data = y_data.iloc[:, 0] - y_data.iloc[:, 1]  # Series
y_data.dropna(inplace=True)
dataset = pd.concat([dataset, y_data], axis=1, sort=True)
dataset.columns = ['close', 'volume', 'open', 'beat_index']
dataset.dropna(inplace=True)
dataset['beat_index'] = np.where(dataset['beat_index'] > 0, 1, 0)

sample_cutoff = int(0.7 * len(dataset))
training_set = dataset.iloc[:sample_cutoff, :].values  # (2548, 3)
testing_set = dataset.iloc[sample_cutoff:, :].values  # (1093, 3)

sc = MinMaxScaler(feature_range=(0, 1))
training_set_scaled = sc.fit_transform(training_set)  # 2549 * 3
testing_set_scaled = sc.fit_transform(testing_set)


X_train = []
y_train = []
X_test = []
y_test = []
for i in range(60, 500):
    X_train.append(training_set_scaled[i-60:i, 0:3])
    y_train.append(training_set_scaled[i, 3])
    if i < 616:
        X_test.append(testing_set_scaled[i-60:i, 0:3])
        y_test.append(testing_set_scaled[i, 3])

X_train, y_train = np.array(X_train), np.array(y_train)
# (2390, 60, 3); (2390, 1)
X_test, y_test = np.array(X_test), np.array(y_test)
# y_train, ytest = to_categorical(y_train), to_categorical(y_test)


# ## Optimizer One: modAL
from scipy.stats import uniform
param_dist = {'learning_rate': uniform(0.000001, 0.001), 'dropout': uniform(0.1, 0.3), 'batch_size': [4, 8, 16, 32]}
BO_object = HyperparamBayesianOptimizer(target_function, param_dist)
start_t = time.time()
BO_object.maximize(size=500, max_iter=10, verbose=2, gp_kernel='Matern', query_strategy=max_PI)
X, X_initial, y_initial = BO_object.X, BO_object.X_initial, BO_object.y_initial
end_t = time.time()
print('size500 para3 time:', (end_t - start_t)/60)

BO_object = HyperparamBayesianOptimizer(target_function, param_dist)
start_t = time.time()
BO_object.maximize(size=1000, max_iter=10, verbose=2, gp_kernel='Matern', query_strategy=max_PI)
X, X_initial, y_initial = BO_object.X, BO_object.X_initial, BO_object.y_initial
end_t = time.time()
print('size1000 para3 time:', (end_t - start_t)/60)

param_dist = {'learning_rate': uniform(0.000001, 0.001), 'dropout': uniform(0.1, 0.3)}
BO_object = HyperparamBayesianOptimizer(target_function, param_dist)
start_t = time.time()
BO_object.maximize(size=500, max_iter=10, verbose=2, gp_kernel='Matern', query_strategy=max_PI)
X, X_initial, y_initial = BO_object.X, BO_object.X_initial, BO_object.y_initial
end_t = time.time()
print('size500 para2 time:', (end_t - start_t)/60)
# y_pred, y_std = BO_object.predict(X, return_std=True)
# y_pred, y_std = y_pred.ravel(), y_std.ravel()
# X_training, y_training = BO_object.X_training, BO_object.y_training
# X_max, y_max = BO_object.opt_X, BO_object.opt_y

