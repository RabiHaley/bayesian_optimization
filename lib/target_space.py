import numpy as np
from .utils import ensure_rng


def _hashable(x):
    """ ensure that an point is hashable by a python dict """
    return tuple(map(float, x))


class TargetSpace(object):
    """
    Holds the param-space coordinates (X) and target values (Y)
    Allows for constant-time appends while ensuring no duplicates are added

    Example
    -------
    >>> def opt_objective(p1, p2, p3, p4):
    >>>     if not isinstance(p3, int) or not isinstance(p4, int):
    >>>         raise Exception("p3/p4 should be an integer!")
    >>>     if p2 == 'plus':
    >>>         return p1 + 2 * p3 * p4
    >>>     elif p2 == 'minus':
    >>>         return p1 - 2 * p3 * p4
    >>>     else:
    >>>         raise Exception('p2 is illegal str!')
    >>> params = {'p1': {'type': 'cont', 'value': [0, 1]}, 'p2': {'type': 'cate', 'value': ['plus', 'minus']}, 'p3': {'type': 'int', 'value': [4, 8, 16, 32]}, 'p4': {'type': 'int', 'value': range(0,10)}}
    >>> BO_object = BayesianOptimization(opt_objective, params, random_state=0, verbose=2)
    """
    def __init__(self, target_func, params, random_state=None):
        """
        Parameters
        ----------
        :param target_func: the target function to be maximized
        :param params: dict
            key: str, name of the parameter
            value: dict{'type': type of the parameter, 'value': lower/upper bounds or values of the parameter}
            'type' can be 'cont' (continuous), 'cate' (categorical, not number), 'int' (discrete integers)
        :param random_state: int, RandomState, or None
            Optional, specify a seed for a random number generator
        """
        self.random_state = ensure_rng(random_state)
        self._n_params = len(params)  # number of params

        sort_order = ['cont', 'int', 'cate']  # give the sorted function an order
        # original names of params (sorted)
        self._raw_keys = sorted(params, key=lambda x: sort_order.index(params[x]['type']))
        # original types of params (same order as raw_keys)
        self._types = [params[i]['type'] for i in self._raw_keys]
        # original values of params (same order as raw_keys)
        # _raw_values will become list as we apply sorted()
        self._raw_values = [sorted(params[i]['value']) for i in self._raw_keys]

        # names of variables (preprocessed, including dummy)
        self._name_var = []
        # indices of variables (preprocessed, including dummy)
        self._index_var = {}  # key: name_var, value: list of indices

        # The function to be optimized
        self.target_func = target_func

        # bounds of variables (preprocessed, lower/upper bound only), type np.ndarray(dtype=object)
        self._bounds = self.preprocess_params(dummy_bound=10)

        # preallocated memory for X and Y points
        self._params = np.empty(shape=(0, len(self._name_var)))
        self._target = np.empty(shape=(0,))

        # keep track of unique points we have seen so far
        self._cache = {}

    def __contains__(self, x):
        return _hashable(x) in self._cache

    def __len__(self):
        assert len(self._params) == len(self._target)
        return len(self._target)

    @property
    def empty(self):
        return len(self) == 0

    @property
    def params(self):
        return self._params

    @property
    def target(self):
        return self._target

    @property
    def dim(self):
        return self._n_params

    @property
    def keys(self):
        return self._raw_keys

    @property
    def bounds(self):
        return self._bounds

    def preprocess_params(self, dummy_bound=10):
        """
        Preprocess param space, transform dummy and integer variables to continuous ones
        Map preprocessed params to raw params (_index_var)
        :param dummy_bound: int
            range of dummy -> continuous, default [-10, 10]
        :return: np.ndarray(shape=(len(_name_var), 2))
            Add dummy vars, num_row = len(_name_var)
            num_row = 2: (lower bound, upper bound)
        """
        self._name_var = []
        self._index_var = {}
        result = np.empty(shape=(0, 2), dtype=object)
        for i in range(self._n_params):
            value = self._raw_values[i]
            tmp_type = self._types[i]
            tmp_name = self._raw_keys[i]
            if tmp_type == 'cont':
                result = np.r_[result, np.array([value])]  # shape(1, 2)
                self._index_var[tmp_name] = [len(self._name_var)]
                self._name_var.append(self._raw_keys[i])
            elif tmp_type == 'int':
                result = np.r_[result, np.array([[value[0], value[-1]]])]
                self._index_var[tmp_name] = [len(self._name_var)]
                self._name_var.append(self._raw_keys[i])
            elif tmp_type == 'cate':
                num_cate = len(value)
                result = np.r_[result, np.repeat([[-dummy_bound, dummy_bound]], num_cate, axis=0)]
                # shape of np.repeat(num_cate, 2)
                self._index_var[tmp_name] = list(range(len(self._name_var), len(self._name_var) + num_cate))
                self._name_var.extend(value)
        return result

    def params_to_array(self, params):
        """
        Convert a dict of param_name: param_value to array
        :param params: dict
            key: parameter name from self._name_var
            value: float
        :return: np.ndarray
        """
        try:
            assert set(params) == set(self._name_var)
        except AssertionError:
            raise ValueError("Parameters' keys ({}) do ".format(sorted(params)) + "not match the expected set of keys ({}).".format(self._name_var))
        return np.asarray([params[key] for key in self._name_var])

    def array_to_params(self, x):
        """
        Convert an array of param_values to dict
        :param x: np.ndarray of parameter values
            in the order of self._name_var
        :return: dict
            key: parameter name from self._name_var
            value: parameter value as float
        """
        try:
            assert len(x) == len(self._name_var)
        except AssertionError:
            raise ValueError("Size of array ({}) is different than the ".format(len(x)) + "expected number of parameters ({}).".format(len(self._name_var)))
        return dict(zip(self._name_var, x))

    def to_raw_params(self, x):
        """
        Convert an array of len(_name_var) params to raw params that can be accepted by target function
        :param x: np.ndarray
            a single point, len(x) = len(_name_var)
        :return: np.ndarray(dtype=object)
            a single point, len = _n_params
            Element types exactly match the input types of target_func
        """
        result = x[:self._n_params].astype(object)
        for i, param_name in enumerate(self._raw_keys):
            if self._types[i] == 'int':
                param_values = self._raw_values[i]
                dist = [abs(k - result[i]) for k in param_values]
                result[i] = int(param_values[np.argmin(dist)])
            elif self._types[i] == 'cate':
                x_values = x[self._index_var[param_name]]
                param_values = self._raw_values[i]
                result[i] = param_values[np.argmax(x_values)]
        return result

    def _as_array(self, x):
        try:
            if isinstance(x, dict):
                x = list(x.values())
            x = np.asarray(x, dtype=object)
        except TypeError:
            x = self.params_to_array(x)

        x = x.ravel()
        try:
            assert x.size == len(self._name_var)
        except AssertionError:
            raise ValueError("Size of array ({}) is different than the ".format(len(x)) + "expected number of parameters ({}).".format(len(self._name_var)))
        return x

    def register(self, params, target):
        """
        Append a point and its target value to the known data.

        :param params : ndarray
            a single point, with len(x) == len(self._name_var)
        :param target : float
            target function value

        :raise KeyError:
            if the point is not unique

        Notes
        -----
        runs in amortized constant time
        """
        x = self._as_array(params)
        if x in self:
            raise KeyError('Data point {} is not unique'.format(x))
        # Insert data into unique dictionary
        self._cache[_hashable(x.ravel())] = target
        self._params = np.concatenate([self._params, x.reshape(1, -1)])
        self._target = np.concatenate([self._target, [target]])

    def probe(self, params):
        """
        Evaulates a single point x, to obtain the value y and then records them as observations.

        :param params : np.ndarray
            a single point, with len(x) == len(self._name_var)

        Notes
        -----
        If x has been previously seen returns a cached value of y.

        :return target : float
            value of target function.
        """
        x = self._as_array(params)

        try:
            target = self._cache[_hashable(x)]
        except KeyError:
            raw_para = self.to_raw_params(x)
            params = dict(zip(self._raw_keys, raw_para))
            target = self.target_func(**params)
            self.register(x, target)
        return target

    def random_sample(self):
        """
        Creates random points within the bounds of the space.

        :return data: np.ndarray
            [num * dim] array points with dimensions corresponding to `self._name_var`
        """
        data = np.empty((1, len(self._name_var)))
        for col, bound in enumerate(self._bounds):
            data.T[col] = self.random_state.uniform(bound[0], bound[1], size=1)
        return data.ravel()

    def varied_max(self):
        """
        Get maximum target value found and corresponding parameters (with dummy).
        :return res: dict
            {'target': maximum value of target_func,
            'params': corresponding dict with para_names and values}
        """
        try:
            res = {'target': self.target.max(), 'params': dict(zip(self._name_var, self.params[self.target.argmax()]))}
        except ValueError:
            res = {}
        return res

    def max(self):
        """
        Get maximum target value found and corresponding original parameters.
        :return res: dict
            {'target': maximum value of target_func,
            'params': corresponding dict with para_names and values}
        """
        try:
            tmp_max = self.varied_max()
            tmp_params = tmp_max['params']
            raw_values = self.to_raw_params(self.params_to_array(tmp_params))
            res = {'target': self.target.max(), 'params': dict(zip(self._raw_keys, raw_values))}
        except ValueError:
            res = {}
        return res

    def varied_res(self):
        """
        Get all target values found and corresponding parameters (with dummy).
        :return res: list of dict
            {'target': value of target_func,
            'params': dict with para_names and values}
        """
        params = [dict(zip(self._name_var, p)) for p in self.params]
        return [{"target": target, "params": param} for target, param in zip(self.target, params)]

    def res(self):
        """
        Get all target values found and corresponding original parameters.
        :return res: list of dict
            {'target': value of target_func,
            'params': dict with para_names and values}
        """
        tmp_res = self.varied_res()
        tmp_params = [i['params'] for i in tmp_res]
        raw_values = [self.to_raw_params(self.params_to_array(i)) for i in tmp_params]
        params = [dict(zip(self._raw_keys, i)) for i in raw_values]
        return [{"target": target, "params": param} for target, param in zip(self.target, params)]

    def set_bounds(self, new_bounds):
        """
        A method that allows changing lower/upper bounds or possible values of parameters

        :param new_bounds : dict
            key: parameter name
            value: dict {'type': ... , 'value': ...}
        """
        for i, key in enumerate(self._raw_keys):
            if key in new_bounds:
                self._raw_values[i] = new_bounds[key]


