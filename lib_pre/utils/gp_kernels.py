from sklearn.gaussian_process.kernels import Matern
import numpy as np


class IntegerMatern(Matern):
    def __init__(self, length_scale=1.0, length_scale_bounds=(1e-5, 1e5), nu=2.5):
        """
        Integer Matern kernel: compatible with interger-valued parameters: an extension of the Matern kernel.
        nu=2.5 is the default model, which is suggested by those who wrote Spearmint to be an appropriate parameter.
        :param length_scale: float or array with shape (n_features,), default: 1.0
            The length scale of the kernel. If a float, an isotropic kernel is
            used. If an array, an anisotropic kernel is used where each dimension
            of l defines the length-scale of the respective feature dimension.
        :param length_scale_bounds: pair of floats >= 0, default: (1e-5, 1e5)
        T   he lower and upper bound on length_scale
        :param nu: float, default: 2.5
            The parameter nu controlling the smoothness of the learned function.
            The smaller nu, the less smooth the approximated function is.
            For nu=inf, the kernel becomes equivalent to the RBF kernel and for nu=0.5
            to the absolute exponential kernel. Important intermediate values are
            nu=1.5 (once differentiable functions) and nu=2.5 (twice differentiable functions).
        """
        super(IntegerMatern, self).__init__(length_scale, length_scale_bounds, nu)

    def __call__(self, X, int_index=None, Y=None, eval_gradient=False):
        """
        Return the kernel k(int(X), Y) and optionally its gradient.
        Parameters
        ----------
        :param X : array, shape (n_samples_X, n_features)
            Left argument of the returned kernel k(X, Y)
        :param int_index: list
            Indices [i] of X where the value of x_i should be an integer.
        :param Y : array, shape (n_samples_Y, n_features), (optional, default=None)
            Right argument of the returned kernel k(X, Y). If None, k(X, X) if evaluated instead.
        :param eval_gradient : bool (optional, default=False)
            Determines whether the gradient with respect to the kernel
            hyperparameter is determined. Only supported when Y is None.
        Returns
        -------
        :return K : array, shape (n_samples_X, n_samples_Y)
            Kernel k(X, Y)

        :return K_gradient : array (opt.), shape (n_samples_X, n_samples_X, n_dims)
            The gradient of the kernel k(X, X) with respect to the
            hyperparameter of the kernel. Only returned when eval_gradient is True.
        """
        int_X = X.copy()
        if int_index:
            for i in int_index:
                int_X[:, i] = np.around(int_X[:, i].astype(np.float64))
        else:
            int_X = np.around(X.astype(np.float64))
        # TODO: np.ndarray.astype() is not able to deal with the following exceptions - np.nan, np.inf, Non
        # TODO: dtype of int_X is np.float64, thus the parameter cannot be viewed as integer in target function
        return super().__call__(X=int_X, Y=Y, eval_gradient=eval_gradient)
