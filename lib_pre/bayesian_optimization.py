import numpy as np
from sklearn.model_selection import ParameterSampler
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern, RBF
from .models.learners import BayesianOptimizer
from .acquisition import max_EI
from .utils.gp_kernels import IntegerMatern


def ensure_rng(random_state=None):
    """
    Create a random number generator.

    Parameters
    ----------
    :param random_state: int, None, np.random.RandomState

    Return
    -------
    :return: np.random.RandomState
    """
    if random_state is None:
        random_state = np.random.RandomState()
    elif isinstance(random_state, int):
        random_state = np.random.RandomState(random_state)
    else:
        assert isinstance(random_state, np.random.RandomState)
    return random_state


class HyperparamBayesianOptimizer:
    def __init__(self, opt_objective, param_dist, random_state=None):
        """
        Initialize a bayesian optimizer (copying modAL).
        Parameters
        ----------
        :param opt_objective: callable
            Target function to be maximized
        :param param_dist: dict
            Dictionary where the keys are parameters and values are distributions from which a parameter is to be sampled.
            Distributions either have to provide a ``rvs`` function to sample from them,
            or can be given as a list of values, where a uniform distribution is assumed.
        :param random_state: int, np.random.RandomState or None (default=None)
            Pseudo random number generator state used for random uniform sampling from lists of possible values instead
            of scipy.stats distributions and for selecting initial X.
        :param verbose: int
            0, 1, 2

        Example:
        -----------
        >>> from scipy.stats import uniform
        >>> def opt_objective(p1, p2, p3):
        >>>     if not isinstance(p3, int):
        >>>         raise Exception("p3 should be an integer!")
        >>>     return - p1 + p2 * p3
        # >>> param_dist = np.array([['p1', np.int32, [0, 1]], ['p2', int, uniform(1, 100)], ['p3', int, [4, 8, 16, 32]]])
        >>> param_dist = {'p1': [0, 1], 'p2': uniform(1, 100), 'p3': [4, 8, 16, 32]}
        >>> BO_object = HyperparamBayesianOptimizer(target_func, param_dist, random_state=0, verbose=2)
        -----------
        """
        # target function to be optimized
        self.opt_objective = opt_objective
        # list(str), name of the parameters
        self._params = sorted(param_dist)
        self._n_params = len(self._params)
        # self._param_value = dict(zip(param_dist[:, 0], param_dist[:, 2]))
        # self._param_type = dict(zip(param_dist[:, 0], param_dist[:, 1]))
        self._param_dist = param_dist
        self._random_state = ensure_rng(random_state)
        self._X = self._X_initial = self._y_initial = None

    def _data_preparation(self, size=100):
        """
        Transform the data to appropriate form so that modAL.BayesianOptimizer can take it as input.
        Generate X, X_initial, y_initial (property)

        Parameters
        ----------
        :param size: int
            dimension of random numbers to be generated.

        Return
        -------
        :return: None
            Results are properties of the class.
        """
        self._random_sample = list(ParameterSampler(self._param_dist, n_iter=size, random_state=self._random_state))
        # format of _random_sample: list of dict
        # [{'p1': 0, 'p2': 60.28446182250183, 'p3': 4},
        #  {'p1': 1, 'p2': 86.79456176227568, 'p3': 32},
        #  {'p1': 1, 'p2': 63.35636967859723, 'p3': 16},
        #  {'p1': 0, 'p2': 30.753460654447228, 'p3': 4}]
        self._X = np.zeros(shape=(size, self._n_params), dtype=object)
        # _X = np.zeros(shape=(10, 3), dtype=[('a', np.float64), ('b', np.float64), ('c', np.int32)])
        for i in range(len(self._params)):
            param_name = self._params[i]
            self._X[:, i] = np.array([k[param_name] for k in self._random_sample])
        rnd_idx = self._random_state.randint(0, size)
        self._X_initial = self._X[rnd_idx, :]
        params = dict(zip(self._params, self._X_initial.tolist()))
        self._X_initial = np.reshape(self._X_initial, (1, -1))
        self._y_initial = self.opt_objective(**params)
        self._y_initial = np.reshape(self._y_initial, (1, -1))
        return

    @property
    def X(self):
        return self._X

    @property
    def X_initial(self):
        return self._X_initial

    @property
    def y_initial(self):
        return self._y_initial

    def _initialize_optimizer(self, size=100, kernel='IntegerMatern', query_strategy=max_EI):
        if self._X_initial is None:
            self._data_preparation(size=size)
        if kernel == "IntegerMatern":
            kernel = IntegerMatern(length_scale=self._n_params, nu=2.5)
        elif kernel == 'Matern':
            kernel = Matern(length_scale=self._n_params, nu=2.5)
        elif kernel == 'RBF':
            kernel = RBF(length_scale=self._n_params)
        regressor = GaussianProcessRegressor(kernel=kernel)
        optimizer = BayesianOptimizer(estimator=regressor, X_training=self._X_initial, y_training=self._y_initial, query_strategy=query_strategy)
        return optimizer

    def maximize(self, size=100, max_iter=30, verbose=2, gp_kernel="IntegerMatern", query_strategy=max_EI):
        """
        Iterate to optimize (maximize) the target function.
        Parameters
        ----------
        :param size: int
            Sample size of X
        :param max_iter: int
            maximum number of iterations
        :param verbose: int
        :param gp_kernel: sklearn.gaussian_process.kernels.Kernel
            Kernel taken as input in GaussianProcessRegressor
        :param query_strategy: callable
            Type of acquisition function

        Return
        -------
        :return: None
            Results shown as properties
        """
        self.optimizer = self._initialize_optimizer(size=size, kernel=gp_kernel, query_strategy=query_strategy)
        for n_query in range(max_iter):
            query_idx, query_x = self.optimizer.query(self._X)
            query_x = query_x.reshape(1, -1)  # shape (1, _n_params)
            params = dict(zip(self._params, query_x[0]))
            query_y = self.opt_objective(**params)
            query_y = query_y.reshape(1, -1)
            self.optimizer.teach(query_x, query_y)
            if verbose == 2:
                pass
        return

    @property
    def max(self):
        return self.optimizer.get_max()

    @property
    def opt_X(self):
        return dict(zip(self._params, self.max[0].tolist()))

    @property
    def opt_y(self):
        return self.max[1][0]

    @property
    def X_training(self):
        return self.optimizer.X_training

    @property
    def y_training(self):
        return self.optimizer.y_training

    def predict(self, X_input, **predict_kwargs):
        return self.optimizer.predict(X_input, **predict_kwargs)


